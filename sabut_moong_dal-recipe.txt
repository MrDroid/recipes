Punjabi Sabut Moong Dal | Green Moong Dal Tadka Recipe

Punjabi Sabut moong dal recipe. Healthy whole green moong dal recipe with punjabi tadka. Perfect to serve with roti chapati paratha or rice.

Prep time  25 mins
Cook time  15 mins
Total time 40 mins
Cuisine: Punjabi
 
Ingredients

    * 1 tsp salt
    * 1 tsp chili powder red
    * 1/2 tsp turmeric powder
    * 2.5 Tbsp ghee for tempering
    * 1 tsp garam masala
    * 1/4 cup onion chopped
    * 2 tomato chopped
    * 1 tsp cumin
    * 2 whole red chilli
    * 1 green chilli chopped
    * 1 tsp ginger chopped
    * 1/5 tsp asafoetida hing
    * lime juice
    * 1 tbsp coriander leaves

    
Instructions

    * Clean whole Moong dal, wash then soak dal in water for few hours.
    * Discard the water in which dal was soaked and wash the dal/lentil once again.
    * Put the Moong dal in a pressure cooker, add 4 cups of water, and salt, red chilli powder and turmeric powder. Close the lid of pressure cooker and place the dal on the gas to cook.
    * After 1 whistle reduce the heat to the lowest and cook dal for another 3-4 whistles. In total Dal should be cooked for 20 Minutes.
    * Turn off the heat, and wait until all of the pressure escapes from the cooker.
    * In the meanwhile prepare the tempering for sabut moong dal tadka.
    * In a small tadka pan heat up the 2Tbsp of ghee and add asafoetida, chopped green chilli, ginger and chopped onion. Cook them till onion are light brown.
    * Now add chopped tomatoes and cook the tomatoes till they are pulpy, add garam masala and mix well.
    * Switch off the heat.
    * Now When pressure is released from cooker, open the lid and mix it well. In case moong dal is not cooked to your preferred consistency cook it with lid open for few more minutes. Or, adjust the water if you feel it is less by adding boiling water.
    * Pour the hot tempering in the pressure cooked dal.
    * Mix it well and serve it in a big bowl.
    * Now in another tadka pan heat up 1/2 tbsp of ghee and add cumin and broken whole red chilli. Allow the cumin seeds to pop and switch off the heat add this hot tadka over the hot dal.
    * Serve this punjabi style sabut moong dal tadka with, Tandoori roti, tandoori paratha, fresh hot paratha or steamed rice.

